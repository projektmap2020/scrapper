const puppeteer = require('puppeteer');
const fs = require('fs');
const request = require('request')
var http = require('http');
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

var urls = process.argv.slice(2);

var i;
for(i = 0; i<urls.length; i++){
    skeniraj(urls[i], "url"+i);
}

function skeniraj(url, name){
(async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.setViewport({
        width: 1920,
        height: 2160,
        deviceScaleFactor: 1,
      });
    await page.goto(url, {waitUntil: 'networkidle0'});

    //Počakamo, da se stran naloži-
    await page.waitFor('#to');

    // nastavimo velikost strani na 20
    const elementHandle = await page.$('#to');
    await elementHandle.type("20");
    await page.$eval('#to', el => el.value = '20');
    await page.keyboard.press('Enter');

    //pridobimo leto za katerega podatki veljajo
    let leto = page.$eval('.h2-subheading', (element) => {
        return element.innerHTML
    });
    leto = (await leto).substring(6,10);
    let letoSt = parseInt(leto);

    let allData = [];
    var i;
    for(i=0; i<25; i++){
        //Pridobi podatke na tej strani
        let vsi = await page.evaluate((letoSt) => {
            let podatkiStran = [];
            let trenutnaStran = document.querySelectorAll('.ui-widget-content');
            trenutnaStran.forEach((podatek) => {
                let trenutni = {};
                try {
                    trenutni.roadName = podatek.querySelector('.l3').innerHTML;
                    trenutni.year = letoSt;
                    trenutni.numberOfVehicles = podatek.querySelector('.l9').innerHTML;
                    trenutni.numberOfVehicles = parseInt(trenutni.numberOfVehicles);
                    trenutni.numberOfMotorbikes = podatek.querySelector('.l10').innerHTML;
                    trenutni.numberOfMotorbikes = parseInt(trenutni.numberOfMotorbikes);
                    trenutni.numberOfCars = podatek.querySelector('.l11').innerHTML;
                    trenutni.numberOfCars = parseInt(trenutni.numberOfCars);
                    trenutni.numberOfBuses = podatek.querySelector('.l12').innerHTML;
                    trenutni.numberOfBuses = parseInt(trenutni.numberOfBuses);
                    let lahkaTov = podatek.querySelector('.l13').innerHTML;
                    let lahkaTovSt = parseInt(lahkaTov);
                    let srednaTov = podatek.querySelector('.l14').innerHTML;
                    let srednaTovSt = parseInt(srednaTov);
                    let tezkaTov = podatek.querySelector('.l15').innerHTML;
                    let tezkaTovSt = parseInt(tezkaTov);
                    let tovPrikolica = podatek.querySelector('.l16').innerHTML;
                    let tovPrikolicaSt = parseInt(tovPrikolica);
                    trenutni.numberOfTrucks = lahkaTovSt + srednaTovSt + tezkaTovSt + tovPrikolicaSt;
                }
                catch (exception){
                    console.dir("Napaka.");
                }
                podatkiStran.push(trenutni);
            });
            return podatkiStran;
        }, letoSt);

        allData.push(vsi);
        //Pojdi na naslednjo stran
        await page.click('.next').then(page.waitForSelector('.alert-success').then(page.waitFor(1000)));
    }
    //Prikaz podatkov v konzoli
    console.dir(allData);

    //Pisanje podatkov v datoteko
    //fs.writeFile ("data/" + name + ".json", JSON.stringify(allData), function(err) {
    //    if (err) throw err;
    //    console.log('complete');
    //    }
    //);

    sendDataToApi(allData);

    await browser.close();
    })();
}

function sendDataToApi(allData){
    var xhr = new XMLHttpRequest();
    xhr.open("POST", 'http://localhost:3000/api/rl', true);

    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            console.log("Podatki poslani.");
        }
    }
    
    xhr.send(JSON.stringify(allData));
}